# index.html - INITIATION


Before starting rest of app:
```
var slSound = require('path/to/node_modules/sl-sound/slSound');
var sls = new slSound();
var files = [
    {name: 'key', filename: 'filename.wav' },
    ...
  ];
const pathToAudioFolder = './app/assets/'
sls.loadFiles(files, pathToAudioFolder).then(function(files){
  // Proceed with rest of app
});
```

The files will be loaded into buffers, and will return a list of all the loaded files.

## to play a file

Call a file by its key which you should have provided at initiation.

```
sls.play('key', loop?)
```

Second argument is optional. If true, the .wav will loop until manually stopped.


## check if playing

```
sls.isPlaying('key');
```

## to stop

```
sls.stop('key');
```

## to stop all

```
sls.stopAll();
```

## return al files

To return a list of all loaded files:

```
sls.getFiles()
```
