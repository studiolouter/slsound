require('path');
const fs = require('fs');
const path = require('path');
const load = require('audio-loader');
var play = require('audio-play');
var player = require('sample-player');
var ac = new AudioContext();
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

function slSound() {
  var slfiles = [];
  this.loadFiles = function(thisFiles, pathToAudioFolder = null) {
    if(typeof thisFiles == 'string'){
      return new Promise(
        function(resolve, reject) {
          fs.readdir(thisFiles, function(err, files){
            if(err){
              console.log(err);
              return "Er is iets fout gegaan: "+err;
            } else {
              const maxCount = files.length
              let fileCount = 0;
              files.forEach(file => {
                let thisFile = {filename:null, buffer: null};
                load(path.join(pathToAudioFolder, file.filename)).then(function(buffer){
                  thisFile.filename = file.filename;
                  thisFile.buffer = buffer;
                  thisFile.name = file.name
                  thisFile.isPlaying = false;
                  var source = ac.createBufferSource();
                  source.buffer = buffer;
                  source.connect(ac.destination);
                  thisFile.source = source;
                  console.log(ac);
                  slfiles.push(thisFile)

                  fileCount++;
                  if(maxCount == fileCount) resolve(slfiles)
                });
                if(fileCount === maxCount) resolve(slfiles)
              })
            }
          })
        }
      )
    } else if(typeof thisFiles == 'object'){
      if(Array.isArray(thisFiles)){
        return new Promise(
          function (resolve, reject){
            const maxCount = thisFiles.length
            let fileCount = 0;
            thisFiles.forEach(file => {
              let thisFile = {filename:null, buffer: null};
              load(path.join(__dirname, pathToAudioFolder, file.filename)).then(function(buffer){
                thisFile.filename = file.filename;
                thisFile.buffer = buffer;
                thisFile.name = file.name
                thisFile.isPlaying = false;
                var source = ac.createBufferSource();
                source.buffer = buffer;
                source.connect(ac.destination);
                thisFile.source = source;
                slfiles.push(thisFile)
                fileCount++;
                if(maxCount == fileCount) resolve(slfiles)
              });
              if(fileCount === maxCount) resolve(slfiles)
            })
          }
        )
      } else {
        thisFiles.forEach(file => {
          console.log(file);
        })
      }
    }

  }

  this.getFiles = function(){
    return slfiles;
  }
  this.isPlaying = function(filename){
    let playBool = false;
    slfiles.forEach( file => {
      if(file.name == filename){
        if(file.isPlaying){
          playBool = true;
        }
      }
    })
    return playBool;
  }
  this.play = function(filename, loop = false){
    slfiles.forEach( ( file , index, object ) => {
      if(file.name === filename){
        var source = ac.createBufferSource();
        source.buffer = file.buffer;
        source.loop = loop;
        source.connect(ac.destination);
        file.source = source;
        try{
          file.source.stop();
          file.isPlaying = false;
        } catch (err){

        }
        file.source.start();
        file.isPlaying = true;
        file.source.onended = function(event){
          file.isPlaying = false;
        }
      }
    });
  }

  this.stop = function(fileToStop){
    slfiles.forEach( ( playingFile , index, object ) => {
      if(playingFile.isPlaying == true && playingFile.name == fileToStop){
        playingFile.source.stop();
        playingFile.isPlaying = false;
      }
    });
  }

  this.stopAll = function(){
    slfiles.forEach( ( playingFile , index, object ) => {
      if(playingFile.isPlaying == true){
        playingFile.source.stop();
        playingFile.isPlaying = false;
      }
    });
  }
}

module.exports = slSound;
